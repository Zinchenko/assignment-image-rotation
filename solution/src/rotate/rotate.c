#include "rotate.h"

struct image *rotate_image(struct image* original){

  struct image *rotated = image_create(original->height, original->width);
  rotated->pixels = malloc(sizeof(struct pixel)*rotated->height*rotated->width);
for (size_t i = 0; i < rotated->height; i++) {
    for (size_t j = 0; j < rotated->width; j++) {
      struct pixel pixel = original->pixels[(rotated->width - j - 1) * original->width + i];
      rotated->pixels[i * rotated->width + j] = pixel;
    }
  }
  return rotated;
}
