#include "file.h"

int open_file(FILE **file, const char *pathname, const char *mode) {
    *file = fopen(pathname, mode);
    if (!*file) {
        return 1;
    }else{
        return 0;
    }
}

int close_file(FILE **file) {
  if (fclose(*file) == EOF){
    return 1;
  }else{
    free(file);
    return 0;
  }
}

