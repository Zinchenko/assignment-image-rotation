#include "file.h"
#include "bmp.h"
#include "rotate.h"
#include <stdlib.h>

int main(int argc, char **argv) {

    if (argc < 3) {
        fprintf(stderr, "Wrong number of arguments.");
        return -1;
    }

    FILE **input_bmp = malloc(sizeof(FILE*));
    if (input_bmp ==NULL) {
        return -1;
    }
    if(open_file(input_bmp, argv[1], "r") != 0){
        fprintf(stderr, "Failed to open file on read");
        return -1;
    }

    FILE **output_bmp = malloc(sizeof(FILE*));
    if (output_bmp ==NULL) {
        return -1;
    }
    if(open_file(output_bmp, argv[2], "w") != 0){
        fprintf(stderr, "Failed to open file on write");
        close_file(input_bmp);
        return -1;
    }

    struct image *original_image = image_create(0, 0);
    if(bmp_to_image(*input_bmp, original_image) != 0){
        fprintf(stderr, "Failed to create image ");
        image_destroy(original_image);
        close_file(input_bmp);
        close_file(output_bmp);
        return -1;
    }
    close_file(input_bmp);
    
    struct image* rotated_image = rotate_image(original_image);
    if(image_to_bmp(*output_bmp, rotated_image) != 0){
        fprintf(stderr, "Failed to create bmp");
        close_file(output_bmp);
        image_destroy(rotated_image);
        return -1;
    }
    close_file(output_bmp);

    image_destroy(original_image);
    image_destroy(rotated_image);

    return 0;
}

