#include "image.h"

struct image *image_create(uint64_t width, uint64_t height) {
  struct image *image = malloc(sizeof(struct image));
  image->width = width;
  image->height = height;
  return image;
}

void image_destroy(struct image *image) { 
  free(image->pixels);
  free(image);
}

size_t get_height(struct image *image) {
    return image->height;
}

size_t get_width(struct image *image) {
    return image->width;
}
