 #include "bmp.h"

#define DECLARE_FIELD(type, name) type name;

#undef FOR_HEADER
#undef DECLARE_FIELD

static inline uint32_t get_padding(uint32_t width) { return width % 4; }

static struct header generate_header(uint32_t width, uint32_t height) {
  const uint32_t head_size = sizeof(struct header);
  const uint32_t img_size = sizeof(struct pixel)
            * height * (width + get_padding(width));
  const uint32_t file_size = head_size + img_size;
    return (struct header) {
            .signature = 0x4d42,
            .file_size = file_size,
            .reserved = 0,
            .data_offset = sizeof(struct header),
            .header_size = 40,
            .width = width,
            .height = height,
            .planes = 1,
            .bits_per_pixel = 24,
            .compression = 0,
            .image_size = 0,
            .x_pixels_per_m = 0,
            .y_pixels_per_m = 0,
            .colors_used = 0,
            .important_colors = 0
  };
}

int read_header(FILE *in, struct header *header) {
    rewind(in);
    if(fread(header, sizeof(struct header), 1, in) == 0) {
      return 1;
    }
    return 0;
}

int read_image(FILE *in, struct image *img) {
  for (uint32_t i = 0; i < img->height; i++) {
    if (fread(&img->pixels[i * img->width], sizeof(struct pixel), img->width, in)
            < img->width || fseek(in, get_padding(img->width), SEEK_CUR) != 0) {
      return 1;
    }
  }
  return 0;
}

int bmp_to_image(FILE *in, struct image *img) {
  struct header header = {0};
  if(read_header(in, &header) != 0){return 1;}
  img->width = header.width;
  img->height = header.height;
  img->pixels = malloc(sizeof(struct pixel)*img->height*img->width);
  if(img->pixels == NULL) {return 1;}
  if( read_image(in, img) != 0){return 1;}
  return 0;
}

int image_to_bmp(FILE *out, struct image const *img) {
  const struct header header = generate_header(img->width, img->height);
  if (fwrite(&header, sizeof(struct header), 1, out)==0) {
    return 1;
  }
  for (uint32_t o = 0, i = 0; i < img->height; i++) {
    if (fwrite(&img->pixels[i * img->width], sizeof(struct pixel), img->width, out)
            < header.width || fwrite(&o, get_padding(img->width), 1, out) < 1) {return 1;}
  }
  return 0;
}


