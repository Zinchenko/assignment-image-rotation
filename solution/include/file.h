#ifndef FILE_H
#define FILE_H

#include <stdio.h>
#include <stdlib.h>

int open_file(FILE **file_link, const char *pathname, const char *mode);

int close_file(FILE **file_link);

#endif

