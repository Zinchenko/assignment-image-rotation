#ifndef ROTATE_H
#define ROTATE_H

#include "image.h"
#include <inttypes.h> 
#include <stddef.h>
#include <stdint.h> 
#include <stdlib.h> 

struct image *rotate_image(struct image* original);

#endif

