#ifndef BMP_H
#define BMP_H

#include "image.h"
#include <inttypes.h>
#include <stddef.h>
#include <stdio.h>
#include <stdlib.h>
#include  <stdint.h>

struct __attribute__((packed)) header
{
        uint16_t signature;
    uint32_t file_size;
    uint32_t reserved;
    uint32_t data_offset;
    uint32_t header_size;
    uint32_t width;
    uint32_t height;
    uint16_t planes;
    uint16_t bits_per_pixel;
    uint32_t compression;
    uint32_t image_size;
    uint32_t x_pixels_per_m;
    uint32_t y_pixels_per_m;
    uint32_t colors_used;
    uint32_t important_colors;
};


int bmp_to_image(FILE *in, struct image *img);

int image_to_bmp(FILE *out, struct image const *img);

#endif

